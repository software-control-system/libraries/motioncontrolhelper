//---------------------------------------------------------------------------
// Project  : MotionControlHelper
// File     : MotionControlHelper.cpp
//---------------------------------------------------------------------------
//
// Copyright    : (c) Synchrotron SOLEIL, 2014
//---------------------------------------------------------------------------
//
/*! \class       MotionControlHelper
 *  \brief
*/
//
//---------------------------------------------------------------------------

#include "MotionControlHelper.h"
#include <yat4tango/AttributeGroup.h>

namespace mch
{

//---------------------------------------------------------------------------
/// Constructor
//---------------------------------------------------------------------------
MotionControlHelper::MotionControlHelper()
{
    m_bAllowMove = true;
    m_is_verbose = false;
    m_statuses.push_back("ON");
    m_statuses.push_back("OFF");
    m_statuses.push_back("CLOSE");
    m_statuses.push_back("OPEN");
    m_statuses.push_back("INSERT");
    m_statuses.push_back("EXTRACT");
    m_statuses.push_back("MOVING");
    m_statuses.push_back("STANDBY");
    m_statuses.push_back("FAULT");
    m_statuses.push_back("INIT");
    m_statuses.push_back("RUNNING");
    m_statuses.push_back("ALARM");
    m_statuses.push_back("DISABLE");
    m_statuses.push_back("UNKNOWN");
}


//---------------------------------------------------------------------------
/// Destructor
//---------------------------------------------------------------------------
MotionControlHelper::~MotionControlHelper()
{
    Reset();
}


//---------------------------------------------------------------------------
/// Destory all axes and remove selections
//---------------------------------------------------------------------------
void MotionControlHelper::Reset()
{
    list<MotionControlAxis*>::iterator iter = m_listAxis.begin();
    while (m_listAxis.end() != iter)
    {
        delete(*iter);
        ++iter;
    }
    m_listAxis.clear();
    m_listSelectedAxis.clear();
}


//---------------------------------------------------------------------------
/// Defines the state of the Move allowance
//---------------------------------------------------------------------------
void MotionControlHelper::AllowMove(const bool bAllow) ///< [in] true to allow movement, false to disable
{
    m_bAllowMove = bAllow;
}


//---------------------------------------------------------------------------
/// Defines a new axis
//---------------------------------------------------------------------------
void MotionControlHelper::DefineAxis(const std::string& axisName,        ///< [in] user name for the axis
                                     const std::string& attributeName)   ///< [in] full attribute name referenced
{
    if (NULL == GetAxis(axisName))
    {
        bool bFound = false;
        list<MotionControlAxis*>::const_iterator iter = m_listAxis.begin();
        while (!bFound && (m_listAxis.end()!=iter))
        {
            MotionControlAxis* pAxis = *iter;
            if (pAxis->GetAttributeName() == attributeName)
            {
                // Exception
                std::string sExceptionMessage;
                sExceptionMessage = "axis " + pAxis->GetName() + " already defined for the attribute "+attributeName;
                Tango::Except::throw_exception("TANGO_DEVICE_ERROR", sExceptionMessage.c_str(), "MotionControlHelper::DefineAxis");
            }
            ++iter;
        }

        try
        {
            MotionControlAxis* pAxis = new MotionControlAxis(axisName, attributeName);
            m_listAxis.push_back(pAxis);
        }
        catch (Tango::DevFailed& df)
        {
            Tango::Except::re_throw_exception(df, std::string(df.errors[0].reason).c_str(),
                                                  std::string(df.errors[0].desc).c_str(),
                                                  "MotionControlHelper::DefineAxis");
        }
    }
    else
    {
        // Exception
        std::string sExceptionMessage;
        sExceptionMessage = "axis " + axisName + " already defined";
        Tango::Except::throw_exception("TANGO_DEVICE_ERROR", sExceptionMessage.c_str(), "MotionControlHelper::DefineAxis");
    }
}


//---------------------------------------------------------------------------
/// Axis accessor
//---------------------------------------------------------------------------
MotionControlAxis* MotionControlHelper::GetAxis(const std::string& axisName)
{
    MotionControlAxis* pFoundItem = NULL;

    list<MotionControlAxis*>::const_iterator iter = m_listAxis.begin();

    bool bFound = false;
    while ( (m_listAxis.end() != iter) && (!bFound) )
    {
        MotionControlAxis* pItem = *iter;
        if (pItem->GetName() == axisName)
        {
            pFoundItem = pItem;
            bFound = true;
        }
        ++iter;
    }

    return pFoundItem;
}


//---------------------------------------------------------------------------
/// Accessor using brackets operator
//---------------------------------------------------------------------------
MotionControlAxis* MotionControlHelper::operator[] (const std::string& axisName)
{
    MotionControlAxis* pFoundItem = GetAxis(axisName);

    // Exception: Axis not defined
    if (NULL==pFoundItem)
    {
        std::string sExceptionMessage;
        sExceptionMessage = "axis " + axisName + " is not defined";
        Tango::Except::throw_exception("TANGO_DEVICE_ERROR", sExceptionMessage.c_str(), "MotionControlHelper::operator[]");
    }

    return pFoundItem;
}


//---------------------------------------------------------------------------
/// Creates an attribute group and runs the "write" operation
//---------------------------------------------------------------------------
void MotionControlHelper::Move()
{
    if (m_bAllowMove)
    {
        yat4tango::AttributeGroup ag_write;

        vector<Tango::DevDouble> vectValues;

        try
        {
            // Scan the list of selected axies
            list<std::string>::const_iterator iterSelectedAxies = m_listSelectedAxis.begin();
            while (m_listSelectedAxis.end() != iterSelectedAxies )
            {
                // If axis is selected
                MotionControlAxis* pItem = (*this)[*iterSelectedAxies];

                // add the axis attribute name and its value
                ag_write.register_attribute(pItem->GetAttributeName());
                vectValues.push_back(pItem->GetWrite());

                iterSelectedAxies++;
            }

            ag_write.write(vectValues);
        }
        catch(Tango::DevFailed& df)
        {

            Tango::Except::re_throw_exception(df, "TANGO_DEVICE_ERROR",
                                                  std::string(df.errors[0].desc).c_str(),
                                                  "MotionControlHelper::Move");
        }
        catch (...)
        {
            // Exception
            Tango::Except::throw_exception( "TANGO_DEVICE_ERROR", "unhandled exception",  "MotionControlHelper::Read");
        }
    }
}


//---------------------------------------------------------------------------
/// Add the given axis to the name of the selected axies.
//---------------------------------------------------------------------------
void MotionControlHelper::SelectAxis(const std::string& axisName)    ///< [in] user name of the axis to select
{
    if (NULL != (*this)[axisName])
    {
        m_listSelectedAxis.push_back(axisName);
    }
    else
    {
        // Exception
        std::string sExceptionMessage;
        sExceptionMessage = "axis " + axisName + " is not defined";
        Tango::Except::throw_exception( "TANGO_DEVICE_ERROR", sExceptionMessage.c_str(), "MotionControlHelper::SelectAxis");
    }
}


//---------------------------------------------------------------------------
/// Removes the given axis from the list of the selected axes.
//---------------------------------------------------------------------------
void MotionControlHelper::UnSelectAxis(const std::string& axisName)
{
    if (NULL != (*this)[axisName])
    {
        m_listSelectedAxis.remove(axisName);
    }
    else
    {
        // Exception
        std::string sExceptionMessage;
        sExceptionMessage = "axis " + axisName + " is not defined";
        Tango::Except::throw_exception("TANGO_DEVICE_ERROR", sExceptionMessage.c_str(), "MotionControlHelper::UnSelectAxis");
    }
}


//---------------------------------------------------------------------------
/// Clear the list of selected axies.
//---------------------------------------------------------------------------
void MotionControlHelper::Clear()
{
    m_listSelectedAxis.clear();
}


//---------------------------------------------------------------------------
/// Creates an attribute group and starts the "read" operation
//---------------------------------------------------------------------------
void MotionControlHelper::Read()
{
	yat4tango::AttributeGroup ag_read;

    vector<Tango::DevDouble> vectValues;

    try
	{
        // Scan the list of selected axies and build the attributeGroup
        list<std::string>::const_iterator iterSelectedAxies = m_listSelectedAxis.begin();
        while (m_listSelectedAxis.end() != iterSelectedAxies )
        {
            // register the attribute name
            ag_read.register_attribute(((*this)[*iterSelectedAxies])->GetAttributeName());

            iterSelectedAxies++;
        }


        // Read the attributeGroup
        yat4tango::AttributeGroupReplyList agrl = ag_read.read();

        if ( agrl.size() == m_listSelectedAxis.size())
        {
            list<std::string>::const_iterator iterSelectedAxies = m_listSelectedAxis.begin();
            for(size_t r=0; r < agrl.size(); r++)
            {
                // retreive item value
                Tango::DevDouble dDouble;
                if( agrl.at(r) >> dDouble )        // try to extract the value
                {
                    ((*this)[*iterSelectedAxies])->SetReadReply(dDouble);
                }
                else
                {
                    // Exception
                    Tango::Except::throw_exception( "TANGO_DEVICE_ERROR",
                                                    "unable to extract attributegroup value",
                                                    "MotionControlHelper::Read");
                }

                ++iterSelectedAxies;
            }
        }
        else
        {
            // Exception: reply list size is not consistend
            Tango::Except::throw_exception( "TANGO_DEVICE_ERROR", "invalid reply list size",  "MotionControlHelper::Read");
        }
    }
    catch(Tango::DevFailed& df)
    {

        Tango::Except::re_throw_exception(df, "TANGO_DEVICE_ERROR",
                                              std::string(df.errors[0].desc).c_str(),
                                              "MotionControlHelper::Read" );
    }
    catch (...)
    {
        // Exception
        Tango::Except::throw_exception( "TANGO_DEVICE_ERROR", "unhandled exception",  "MotionControlHelper::Read");
    }
}


//---------------------------------------------------------------------------
/// Calls the Stop() method for all registered axes
//---------------------------------------------------------------------------
void MotionControlHelper::Stop(void)
{
    list<MotionControlAxis*>::iterator iter = m_listAxis.begin();
    bool has_errors = false;
    std::string exception_message = "The following errors occured:\n";
    while (m_listAxis.end() != iter)
    {
        try
        {
        (*iter)->Stop();
        }
        catch(Tango::DevFailed& df)
        {
            exception_message = exception_message + string(df.errors[0].desc) + "\n";
            has_errors = true;
        }
        ++iter;
    }

    if (has_errors)
    {
        Tango::Except::throw_exception("TANGO_DEVICE_ERROR", exception_message.c_str(), "MotionControlHelper::Stop");
    }
}

//---------------------------------------------------------------------------
/// State
//---------------------------------------------------------------------------

Tango::DevState MotionControlHelper::State(const bool selected_only)
{
    if (m_is_verbose) cout << "MotionControlHelper::State() entering ..." << endl;

    Tango::DevState state_axes = Tango::UNKNOWN;

    list<MotionControlAxis*> list_axes;

    if (m_is_verbose) cout << "MotionControlHelper::State() create axis list" << endl;
    // Create the list of axes to querry from.
    if (selected_only) // use the list of all registered axes
    {
        list<string>::const_iterator iter_selected_axis = m_listSelectedAxis.begin();
        while (m_listSelectedAxis.end() != iter_selected_axis )
        {
            // register the attribute name
            list_axes.push_back(GetAxis(*iter_selected_axis));

            iter_selected_axis++;
        }
    }
    else               // use selected axes only
    {
        list_axes = m_listAxis;
    }

    // Get the state of the axes.
    unsigned int count_moving  = 0;
    unsigned int count_standby = 0;
    unsigned int count_alarm   = 0;
    unsigned int count_fault   = 0;

    yat::MutexLock scoped_lock(m_status_lock);
    m_status_string = "";

    if (m_is_verbose) cout << "MotionControlHelper::State() gather axis states" << endl;

    list<MotionControlAxis*>::iterator iter = list_axes.begin();
    while (list_axes.end() != iter)
    {
        try
        {
            Tango::DevState state_item = (*iter)->State();
            switch (state_item)
            {
                case Tango::MOVING:  count_moving ++; break;
                case Tango::STANDBY: count_standby++; break;
                ///#TANGODEVIC-1536
                ///- Consider state=INIT of GalilAxis                     
                case Tango::INIT: 
                case Tango::FAULT:   count_fault  ++; break;
                default:             count_alarm  ++;
            }
            m_status_string = m_status_string + (*iter)->GetName() + std::string(" --> ") + StateString(state_item) + std::string("\n");
        }
        catch (Tango::DevFailed& e)
        {
            m_status_string = m_status_string +  (*iter)->GetName() + std::string(": unable to get the state !\n");
            count_fault++;
        }

        ++iter;
    }
    if (m_is_verbose) cout << "MotionControlHelper::State() gather axis states done." << endl;

    // Conditions are checked in reverse order or importance so that the most important state (and status message) overwrites the previous one.
    // Currently, the defined order is:
    // FAULT (most important)
    // MOVING
    // ALARM
    // STANDBY

    if (list_axes.size() == count_standby + count_alarm) // All axes are in stanby or alarm.
    {
        state_axes = Tango::STANDBY;
    }

    if  (0 != count_alarm) // At least an unexpected state encountered.
    {
        state_axes = Tango::ALARM;
    }

    if (0 != count_moving) // At least one is moving.
    {
        state_axes = Tango::MOVING;
    }

    if (0 != count_fault) // Means at least one state could not be read or is fault.
    {
        state_axes = Tango::FAULT;
    }

    if (m_is_verbose) cout << "MotionControlHelper::State(): " << StateString(state_axes) << endl;

    return state_axes;
}


//---------------------------------------------------------------------------
/// Status
//---------------------------------------------------------------------------
std::string MotionControlHelper::Status()
{
    yat::MutexLock scoped_lock(m_status_lock);
    return m_status_string;
}


//---------------------------------------------------------------------------
/// StateString
//---------------------------------------------------------------------------
std::string MotionControlHelper::StateString(const Tango::DevState state)
{
    std::string status;

    if ( (state>=Tango::ON) && (state<=Tango::UNKNOWN))
    {
        status = m_statuses.at(state);
    }
    return status;
}

void MotionControlHelper::SetVerbose(bool verbose)
{
    m_is_verbose = verbose;
}

} // namespace mch