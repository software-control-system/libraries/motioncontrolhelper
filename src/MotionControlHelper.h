//---------------------------------------------------------------------------
// Project  : MotionControlHelper
// File     : MotionControlHelper.h
//---------------------------------------------------------------------------
//
// Copyright    : (c) Synchrotron SOLEIL, 2014
//---------------------------------------------------------------------------
//
/*! \class       MotionControlHelper
 *  \brief
*/
//
//---------------------------------------------------------------------------

#if !defined(_MOTIONCONTROLHELPER_H)
#define _MOTIONCONTROLHELPER_H

#include "MotionControlAxis.h"
#include <yat/threading/Mutex.h>
#include <list>

namespace mch
{

class MotionControlHelper
{
public:
    MotionControlHelper();
    ~MotionControlHelper();

    /// Accessor using brackets operator
    MotionControlAxis* operator[] (const string&);

    /// Defines a new axis
	void DefineAxis(const string& axisName, const string& attributeName);

    /// Destory all axes and remove selections
    void Reset(void);

    /// Creates an attribute group and runs the "write" operation
	void Move(void);

    /// Creates an attribute group and starts the "read" operation
    void Read(void);

    /// Add the given axis to the list of the selected axes.
    /// (The selected axes are the axes that will be used for Move() and Read() )
	void SelectAxis(const string& axisName);

    /// Removes the given axis from the list of the selected axes.
	void UnSelectAxis(const string& axisName);

    /// Clear the list of selected axies.
	void Clear(void);

    /// Defines the state of the Move allowance
    void AllowMove(const bool bAllow);

    /// Calls the Stop() method for all registered axes
    void Stop(void);

    /// Calls the State() method on all registered axes
    /*!
    @return Tango::MOVING  if at least one state is moving
    @return Tango::FAULT   if at least one state could not be obtained
    @return Tango::STANDBY if all are STANDBY
    @return Tango::ALARM   if at least one state is different from MOVING or STANDBY
    */
    Tango::DevState State(const bool selected_only=false  ///< [in] change the behavior and call State() only on selected axes
                          );

    /// Return a string stream containing the list of the status established by the last call to State()
    std::string Status();

    /// Return the string standing for the state value.
    std::string StateString(const Tango::DevState state ///< [in] state value
                      );

    void SetVerbose(bool verbose);

private:
    /// Axis accessor
    MotionControlAxis* GetAxis(const string& axisName);

    list<MotionControlAxis*> m_listAxis;
    list<std::string>        m_listSelectedAxis;
    bool                     m_bAllowMove;

    vector<string>           m_statuses;
    std::string              m_status_string;

    bool                     m_is_verbose;

    yat::Mutex               m_status_lock;
    yat::Mutex               m_string_lock;
};

}

#endif  //_MOTIONCONTROLHELPER_H
