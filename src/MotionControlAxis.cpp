//---------------------------------------------------------------------------
// Project  : MotionControlHelper
// File     : MotionControlAxis.cpp
//---------------------------------------------------------------------------
//
// Copyright    : (c) Synchrotron SOLEIL, 2014
//---------------------------------------------------------------------------
//
/*! \class       MotionControlAxis
 *  \brief
 */
//
//---------------------------------------------------------------------------

#include "MotionControlAxis.h"

namespace mch
{

//---------------------------------------------------------------------------
/// Constructor
//---------------------------------------------------------------------------
MotionControlAxis::MotionControlAxis(const string& name,            ///< [in] user axis name  (ex: "Rs1", "Tz2" ... )
									 const string& attributeName)   ///< [in] full attribute name (ex: /user/mono/dev_Rs1/position
{
	m_name          = name;
	m_attributeName = attributeName;
	m_writeValue    = 0.0;
	m_readValue     = 0.0;

	m_devicename 		  = m_attributeName.substr(0, m_attributeName.rfind("/"));
	m_attributename_short = m_attributeName.substr(m_attributeName.rfind("/")+1, string::npos);

	m_deviceproxyhelper = new Tango::DeviceProxyHelper(m_devicename);
}


//---------------------------------------------------------------------------
/// Destructor
//---------------------------------------------------------------------------
MotionControlAxis::~MotionControlAxis()
{
	delete m_deviceproxyhelper;
}


//---------------------------------------------------------------------------
/// Assign a value to write on the axis
//---------------------------------------------------------------------------
void MotionControlAxis::SetWrite(const Tango::DevDouble value) ///< [in] value to set as writeValue
{
	m_writeValue = value;
}

//---------------------------------------------------------------------------
/// Get the value that was used to write on this axis
//---------------------------------------------------------------------------
Tango::DevDouble MotionControlAxis::GetWrite()
{
	return m_writeValue;
}


//---------------------------------------------------------------------------
/// Update the last read value for the axis . i.e this is called by MotionControlHelper.Read()
//---------------------------------------------------------------------------
void MotionControlAxis::SetReadReply(const Tango::DevDouble value) ///< [in] value to set as readValue
{
	m_readValue = value;
}


//---------------------------------------------------------------------------
/// Create an attribute group for this axis only and read the attribute from it
//---------------------------------------------------------------------------
double MotionControlAxis::Read()
{
	yat4tango::AttributeGroup ag_read;

	try
	{
		// Add the attribute name
		ag_read.register_attribute(m_attributeName);

		// Read the attribute group
		yat4tango::AttributeGroupReplyList agrl = ag_read.read();

		if (!agrl.has_failed())
		{
			if (1 == agrl.size())             // reply list size should be one element
			{
				Tango::DevDouble ddouble;
				if(agrl.at(0) >> ddouble )     // try to extract the value
				{
					m_readValue = ddouble;
				}
				else
				{
					// Exception: extract failed
					Tango::Except::throw_exception
						( "TANGO_DEVICE_ERROR", "unable to extract attributegroup value",  "MotionControlAxis::Read");
				}
			}
			else
			{
				// Exception: invalid list size
				Tango::Except::throw_exception
					( "TANGO_DEVICE_ERROR", "unexpected attributegroup reply list size",  "MotionControlAxis::Read");
			}
		}
		else
		{
			// Exception: attribute groupe read failed
			Tango::Except::throw_exception
				( "TANGO_DEVICE_ERROR", "attributegroup read has failed",  "MotionControlAxis::Read");
		}
	}
	catch(Tango::DevFailed& df)
	{
		Tango::Except::re_throw_exception(df, "TANGO_DEVICE_ERROR", string(df.errors[0].desc).c_str(), "MotionControlAxis::Read" );
	}
	catch (...)
	{
		// Exception
		Tango::Except::throw_exception( "TANGO_DEVICE_ERROR", "unhandled exception", "MotionControlAxis::Read");
	}

	return m_readValue;
}



//---------------------------------------------------------------------------
// ReadW
//---------------------------------------------------------------------------
Tango::DevDouble MotionControlAxis::ReadW()
{
	double value = NAN;
	try
	{
		m_deviceproxyhelper->read_attribute_w(m_attributename_short.c_str(), value);
	}
	catch(Tango::DevFailed& df)
	{
		Tango::Except::re_throw_exception(df, "TANGO_DEVICE_ERROR", string(df.errors[0].desc).c_str(), "MotionControlAxis::ReadW" );
	}
	catch (...)
	{
		Tango::Except::throw_exception( "TANGO_DEVICE_ERROR", "unhandled exception",  "MotionControlAxis::Read");
	}

	return value;
}


//---------------------------------------------------------------------------
/// Get the last value for this axis obtained by a global read on the MotionControlHelpr ("asynchronous" read)
//---------------------------------------------------------------------------
Tango::DevDouble MotionControlAxis::ReadReply()
{
	return m_readValue;
}

//---------------------------------------------------------------------------
/// Get the user name of the axis
//---------------------------------------------------------------------------
string MotionControlAxis::GetName()
{
	return m_name;
}

//---------------------------------------------------------------------------
/// Get the name of tango attribute defined for this axis
//---------------------------------------------------------------------------
string MotionControlAxis::GetAttributeName()
{
	return m_attributeName;
}

//---------------------------------------------------------------------------
/// Get the state of the device associated to the attribute of this axis
//---------------------------------------------------------------------------
Tango::DevState MotionControlAxis::State(void)
{
	Tango::DevState state;
	try
	{
		yat4tango::AttributeGroup ag_proxies;
		ag_proxies.register_attribute(m_attributeName);

		Tango::DeviceProxy* pProxy = GetDeviceProxy(ag_proxies);
		state = pProxy->state();
	}
	catch(Tango::DevFailed& df)
	{
		Tango::Except::re_throw_exception(df, "TANGO_DEVICE_ERROR", string(df.errors[0].desc).c_str(), "MotionControlAxis::Read" );
	}
	catch (...)
	{
		Tango::Except::throw_exception( "TANGO_DEVICE_ERROR", "unhandled exception",  "MotionControlAxis::State");
	}

	return state;
}


//---------------------------------------------------------------------------
/// Get the status string of the device associated to the attribute of this axis
//---------------------------------------------------------------------------
string MotionControlAxis::Status(void)
{
	string status;
	try
	{
		yat4tango::AttributeGroup ag_proxies;
		ag_proxies.register_attribute(m_attributeName);

		Tango::DeviceProxy* pProxy = GetDeviceProxy(ag_proxies);
		status = pProxy->status();
	}
	catch(Tango::DevFailed& df)
	{
		Tango::Except::re_throw_exception(df, "TANGO_DEVICE_ERROR", string(df.errors[0].desc).c_str(), "MotionControlAxis::Status");
	}
	catch (...)
	{
		Tango::Except::throw_exception( "TANGO_DEVICE_ERROR", "unhandled exception",  "MotionControlAxis::State");
	}

	return status;
}


//---------------------------------------------------------------------------
/// Lauch a stop command to the device associated to the attribute of this axis
//---------------------------------------------------------------------------
void MotionControlAxis::Stop()
{
	try
	{
		m_deviceproxyhelper->command(C_CMD_STOP);
	}
	catch(Tango::DevFailed& df)
	{
		Tango::Except::re_throw_exception(df, "TANGO_DEVICE_ERROR", string(df.errors[0].desc).c_str(), "MotionControlAxis::Stop" );
	}
	catch (...)
	{
		Tango::Except::throw_exception( "TANGO_DEVICE_ERROR", "unhandled exception",  "MotionControlAxis::Stop");
	}
}


//---------------------------------------------------------------------------
/// Get the DeviceProxy object associated to the attribute
//---------------------------------------------------------------------------
Tango::DeviceProxy* MotionControlAxis::GetDeviceProxy(const yat4tango::AttributeGroup& attrGroup) ///< [in] attribute group to work on
{
	Tango::DeviceProxy* proxy_p = NULL;

	try
	{
		std::vector<Tango::DeviceProxy*> vectProxies = attrGroup.ordered_device_proxy_list();

		if (1 == vectProxies.size() )
		{
			proxy_p = vectProxies.at(0);
			if (NULL == proxy_p)
			{
				// Exception: Proxy is NULL
				string exception_message;
				exception_message = "unable to get DeviceProxy to " + m_attributeName;
				Tango::Except::throw_exception( "TANGO_DEVICE_ERROR",
												exception_message.c_str(),
												"MotionControlAxis::GetDeviceProxy");
			}
		}
		else
		{
			// Exception: invalid list size
			Tango::Except::throw_exception( "TANGO_DEVICE_ERROR",
											"unexpected ordered_device_proxy_list size",
											"MotionControlAxis::GetDeviceProxy");
		}
	}
	catch (Tango::DevFailed& df)
	{
		Tango::Except::re_throw_exception(df, string(df.errors[0].reason).c_str(),
											  string(df.errors[0].desc).c_str(),
										  	  string(df.errors[0].origin).c_str() );
	}
	catch (...)
	{
		Tango::Except::throw_exception( "TANGO_DEVICE_ERROR", "unhandled exception",  "MotionControlAxis::GetDeviceProxy");
	}

	return proxy_p;
}


}
