//---------------------------------------------------------------------------
// Project  : MotionControlHelper
// File     : MotionControlAxis.h
//---------------------------------------------------------------------------
//
// Copyright    : (c) Synchrotron SOLEIL, 2014
//---------------------------------------------------------------------------
//
/*! \class       MotionControlAxis
 *  \brief       This class implements the behavior of one defined axis
*/
//
//---------------------------------------------------------------------------

#if !defined(_MOTIONCONTROLAXIS_H)
#define _MOTIONCONTROLAXIS_H

#include <yat4tango/ExceptionHelper.h>
#include <yat4tango/AttributeGroup.h>
#include <DeviceProxyHelper.h>
#include <string>

namespace mch
{

#define C_CMD_STOP   "Stop"
#define C_CMD_STATUS "Status"

class MotionControlHelper;

class MotionControlAxis
{
    friend class MotionControlHelper;

public:
    /// Constructor
    MotionControlAxis(const string&, const string&);
    ~MotionControlAxis();

    /// Assign a value to write on the axis
    void SetWrite(const Tango::DevDouble value);

    /// Get the value that was used to write on this axis
    Tango::DevDouble GetWrite() ;

    /// Create an attribute group for this axis only and read the attribute from it ("synchronous" read)
	Tango::DevDouble Read();

    /// Read the write part of the attribute (synchronous read via DeviceProxy)
    Tango::DevDouble ReadW();

    /// Get the last value for this axis obtained by a global read on the MotionControlHelpr ("asynchronous" read)
	Tango::DevDouble ReadReply();

    /// Get the user name of the axis
    std::string GetName();

    /// Get the name of tango attribute defined for this axis
    std::string GetAttributeName();


    /// Get the state of the device associated to the attribute of this axis
    Tango::DevState State(void);

    /// Return the tango status string of the device associated to the axis attribute
    std::string Status(void);

    /// Sends a "stop" command to the device associated to the axis attribute
    void Stop(void);

private:
    /// Assign a value to the axis
    void SetReadReply(const Tango::DevDouble value);

    /// Return the DeviceProxy object of device associated to the axis attribute
    Tango::DeviceProxy* GetDeviceProxy(const yat4tango::AttributeGroup& attrGroup);

    Tango::DeviceProxyHelper* m_deviceproxyhelper;

    std::string           m_name;
    std::string           m_attributeName;
	Tango::DevDouble m_writeValue;
    Tango::DevDouble m_readValue;

    std::string m_devicename;
    std::string m_attributename_short;
};


} // namespace mch

#endif  //_MOTIONCONTROLAXIS_H
