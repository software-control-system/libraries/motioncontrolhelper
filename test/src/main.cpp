//============================================================================
// Name        : main.cpp
// Author      : Arafat Noureddine / William Bouladoux
// Version     :
// Copyright   :
// Description :
//============================================================================


#include <iostream>
#include <fstream>
#include <stdlib.h>
#include <math.h>

#include <yat4tango/LogHelper.h>
#include <yat4tango/ExceptionHelper.h>
#include <MotionControlHelper.h>
#include <yat/time/Timer.h>

using namespace std;
using namespace mch;

#ifdef CONFIG_SOLEIL

	#define AXIS_1_NAME				"a.pos"
	#define AXIS_2_NAME				"a.velo"
	#define AXIS_3_NAME				"b.pos"

	#define ATTRIBUTE_1_NAME		"flo/op/motor.a/position"
	#define ATTRIBUTE_2_NAME		"flo/op/motor.a/velocity"
	#define ATTRIBUTE_3_NAME		"flo/op/motor.b/position"

	#define ATTRIBUTE_TEST1_NAME	"dummy/dummy/dummy/position"
	#define ATTRIBUTE_TEST2_NAME	"flo/op/motor.a/dummy"

#else //CONFIG_MEDIANE

	#define AXIS_1_NAME				"rs2"
	#define AXIS_2_NAME				"tz2"
	#define AXIS_3_NAME				"theta"

	#define ATTRIBUTE_1_NAME		"william/mono/simu_rs2/position"
	#define ATTRIBUTE_2_NAME		"william/mono/simu_rx2/position"
	#define ATTRIBUTE_3_NAME		"william/mono/simu_theta/position"

	#define ATTRIBUTE_TEST1_NAME	"william/mono/simu_rs2/dummy"
	#define ATTRIBUTE_TEST2_NAME	"dummy/dummy/dummy/position"
#endif

//----------------------------------------------------------------------------------------------------------
int main(int,char *[])
{


	cout << "[BEGIN]" << endl;

	try
	{
		MotionControlHelper mctrlh;

		yat::Timer Timecounter;
		// Define all axis we need
		cout << "\nDefine Grouped Axis (3 axis)" << endl;
		cout << "----------------------------------------------" << endl;
		Timecounter.restart();		
		mctrlh.DefineAxis(AXIS_1_NAME, ATTRIBUTE_1_NAME);
		mctrlh.DefineAxis(AXIS_2_NAME,	ATTRIBUTE_2_NAME);
		mctrlh.DefineAxis(AXIS_3_NAME, ATTRIBUTE_3_NAME);
		cout << "Duration DefineAxis(3 axis): " << Timecounter.elapsed_msec() << " ms"<<endl;

		// Select which axis we want to use 
		cout << "\nSelect Axis (3 axis)" << endl;
		cout << "--------------------------------------------------" << endl;
		Timecounter.restart();
		mctrlh.SelectAxis(AXIS_1_NAME);
		mctrlh.SelectAxis(AXIS_2_NAME);
		mctrlh.SelectAxis(AXIS_3_NAME);
		cout << "Duration SelectAxis(3 axis): " << Timecounter.elapsed_msec() << " ms"<<endl;

		// Set value of each axis, if axis was selected but not refreshed here, last value will be set.
		cout << "\nSet values (3 axis)" << endl;
		cout << "--------------------------------------------------" << endl;
		Timecounter.restart();		
		mctrlh[AXIS_1_NAME]->SetWrite(10.2);
		mctrlh[AXIS_2_NAME]->SetWrite(1.0);
		mctrlh[AXIS_3_NAME]->SetWrite(5.3);
		cout << "Duration SetWrite(3 axis): " << Timecounter.elapsed_msec() << " ms"<<endl;		

		// Send all values which were set to all selected axis in order to Move() them , this is done through yat4tango::AttributeGroup
		cout << "\nMove all Selected axis (3 axis)" << endl;
		cout << "--------------------------------------------------" << endl;
		Timecounter.restart();		
		mctrlh.Move();
		cout << "Duration Move(3 axis): " << Timecounter.elapsed_msec() << " ms"<<endl;	
		
		/////////////////////////////////////////////////////////
		// Test Synchronous Read on some axis & Check their States
		cout << "\nRead (synchronous) & Check State for (axis 1 & axis 3)" << endl;
		cout << "--------------------------------------------------" << endl;
		do
		{
			Timecounter.restart();					
			cout << "\t--> " << AXIS_1_NAME << " State = "	<< Tango::DevStateName[mctrlh[AXIS_1_NAME]->State()]	<< endl;
			cout << "\t--> " << AXIS_1_NAME << " value = "	<< mctrlh[AXIS_1_NAME]->Read()							<< endl;
			cout << "\t--> " << AXIS_3_NAME << " State = "	<< Tango::DevStateName[mctrlh[AXIS_3_NAME]->State()]	<< endl;
			cout << "\t--> " << AXIS_3_NAME << " value = "	<< mctrlh[AXIS_3_NAME]->Read()							<< endl;
			cout << "Duration Read Synchronous (2 axis): "  << Timecounter.elapsed_msec() << " ms"<<endl;
			sleep(1);
		}
		while ( mctrlh[AXIS_1_NAME]->State() == Tango::MOVING || mctrlh[AXIS_3_NAME]->State() == Tango::MOVING);


		/////////////////////////////////////////////////////////
		// Read all selected axis at once
		cout << "\nRead many attributes at once (read asynchronous (axis 1 & axis 3))" << endl;
		cout << "--------------------------------------------------" << endl;
		// Change axis selection
		mctrlh.Clear();
		mctrlh.SelectAxis(AXIS_1_NAME);
		mctrlh.SelectAxis(AXIS_3_NAME);
		
		Timecounter.restart();
		mctrlh.Read(); // read all

		cout << "\t--> " << AXIS_1_NAME << " value = " << mctrlh[AXIS_1_NAME]->ReadReply() << endl;
		cout << "\t--> " << AXIS_3_NAME << " value = " << mctrlh[AXIS_3_NAME]->ReadReply() << endl;
		cout << "Duration Read Asynchronous (2 axis): " << Timecounter.elapsed_msec() << " ms"<<endl;	

		/////////////////////////////////////////////////////////
		// State/Status
		cout << "\nGet [STATE/STATUS] for all Selected Axis (3 axis)" << endl;
		cout << "--------------------------------------------------" << endl;
		Timecounter.restart();		
		cout << "\t--> " << AXIS_1_NAME << "	[State/Status] [" << Tango::DevStateName[mctrlh[AXIS_1_NAME]->State()]   << "/" << mctrlh[AXIS_1_NAME]->Status()   << "]" << endl;
		cout << "\t--> " << AXIS_2_NAME << "	[State/Status] [" << Tango::DevStateName[mctrlh[AXIS_2_NAME]->State()]   << "/" << mctrlh[AXIS_2_NAME]->Status()   << "]" << endl;
		cout << "\t--> " << AXIS_3_NAME << "	[State/Status] [" << Tango::DevStateName[mctrlh[AXIS_3_NAME]->State()]   << "/" << mctrlh[AXIS_3_NAME]->Status()   << "]" << endl;
		cout << "Duration State/Status (3 axis): " << Timecounter.elapsed_msec() << " ms"<<endl;

		/////////////////////////////////////////////////////////
		// Stop an axis
		cout << "\nStop one Axis (axis 1)" << endl;
		cout << "--------------------------------------------------" << endl;
		mctrlh.Clear();
		mctrlh.SelectAxis(AXIS_1_NAME);
		mctrlh[AXIS_1_NAME]->SetWrite(2);
		mctrlh.Move();
		sleep(2);

		cout << "-- Call Stop() on the axis" << endl;
		Timecounter.restart();			
		mctrlh[AXIS_1_NAME]->Stop();
		cout << "Duration Stop (1 axis): " << Timecounter.elapsed_msec() << " ms"<<endl;

		cout << "-- Axis Must be STANDBY after the Stop()" << endl;
		cout << "\t--> " << AXIS_1_NAME << " State = " << Tango::DevStateName[mctrlh[AXIS_1_NAME]->State()] << endl;
		cout << "\t--> " << AXIS_1_NAME << " value = " << mctrlh[AXIS_1_NAME]->Read() << endl;

		/////////////////////////////////////////////////////////
		// Exceptions Handling
		cout << "\nExceptions management" << endl;
		cout << "--------------------------------------------------" << endl;

		//
		try
		{
			cout << "-- Exception if Define an already existant axis" << endl;
			MotionControlHelper mctrldummy;
			mctrldummy.DefineAxis(AXIS_1_NAME, ATTRIBUTE_1_NAME);
			mctrldummy.DefineAxis(AXIS_1_NAME, ATTRIBUTE_1_NAME);
		}
		catch(Tango::DevFailed& df)
		{
			for(unsigned i = 0;i < df.errors.length();i++)
			{
				cout << "Origin\t: " << df.errors[i].origin << endl;
				cout << "Desc\t: "   << df.errors[i].desc   << endl;
				cout << "Reason\t: " << df.errors[i].reason << endl;
			}
		}

		//
		try
		{
			cout << "-- Exception if Select an inexistant axis" << endl;
			MotionControlHelper mctrldummy;
			mctrldummy.DefineAxis(AXIS_1_NAME, ATTRIBUTE_1_NAME);
			mctrldummy.Clear();
			mctrldummy.SelectAxis("shadow_axis");
		}
		catch(Tango::DevFailed& df)
		{
			for(unsigned i = 0;i < df.errors.length();i++)
			{
				cout << "Origin\t: " << df.errors[i].origin << endl;
				cout << "Desc\t: "   << df.errors[i].desc   << endl;
				cout << "Reason\t: " << df.errors[i].reason << endl;
			}
		}

		//
		try
		{
			cout << "-- Exception if Call on inexistant axis" << endl;
			MotionControlHelper mctrldummy;
			mctrldummy.DefineAxis(AXIS_1_NAME, ATTRIBUTE_1_NAME);
			mctrldummy.Clear();
			mctrldummy.SelectAxis(AXIS_1_NAME);
			cout << "\t--> " << AXIS_1_NAME << " State = " << Tango::DevStateName[mctrldummy["shadow_axis"]->State()] << endl;
		}
		catch(Tango::DevFailed& df)
		{
			for(unsigned i = 0;i < df.errors.length();i++)
			{
				cout << "Origin\t: " << df.errors[i].origin << endl;
				cout << "Desc\t: "   << df.errors[i].desc   << endl;
				cout << "Reason\t: " << df.errors[i].reason << endl;
			}
		}

		//
		try
		{
			cout << "-- Exception if Define an inexistant device" << endl;
			MotionControlHelper mctrldummy;
			mctrldummy.DefineAxis(AXIS_1_NAME, ATTRIBUTE_TEST1_NAME);
			mctrldummy.Clear();
			mctrldummy.SelectAxis(AXIS_1_NAME);
			cout << "\t--> " << AXIS_1_NAME << " State = " << Tango::DevStateName[mctrldummy[AXIS_1_NAME]->State()] << endl;
			cout << "\t--> " << AXIS_1_NAME << " value = " << mctrldummy[AXIS_1_NAME]->Read() << endl;
		}
		catch(Tango::DevFailed& df)
		{
			for(unsigned i = 0;i < df.errors.length();i++)
			{
				cout << "Origin\t: " << df.errors[i].origin << endl;
				cout << "Desc\t: "   << df.errors[i].desc   << endl;
				cout << "Reason\t: " << df.errors[i].reason << endl;
			}
		}

		//
		try
		{
			cout << "-- Exception if Define an inexistant attribute" << endl;
			MotionControlHelper mctrldummy;
			mctrldummy.DefineAxis(AXIS_1_NAME,  ATTRIBUTE_TEST2_NAME);
			mctrldummy.Clear();
			mctrldummy.SelectAxis(AXIS_1_NAME);
			cout << "\t--> " << AXIS_1_NAME << " State = " << Tango::DevStateName[mctrldummy[AXIS_1_NAME]->State()] << endl;
			cout << "\t--> " << AXIS_1_NAME << " value = " << mctrldummy[AXIS_1_NAME]->Read() << endl;
		}
		catch(Tango::DevFailed& df)
		{
			for(unsigned i = 0;i < df.errors.length();i++)
			{
				cout << "Origin\t: " << df.errors[i].origin << endl;
				cout << "Desc\t: "   << df.errors[i].desc   << endl;
				cout << "Reason\t: " << df.errors[i].reason << endl;
			}
		}

		/////////////////////////////////////////////////////////
		// Compute Performancies
		cout << "\nCompute Performancies " << endl;
		cout << "--------------------------------------------------" << endl;
		int iDuration_ms = 500;
		int iCount = 0;
		// Read all attributes Loop during 1 second		
		cout << "-- MotionControlHelper: Read (async for 2 attributes) for " << iDuration_ms << " milli seconds ..." << endl;
		mctrlh.Clear();
		mctrlh.SelectAxis(AXIS_1_NAME);
		mctrlh.SelectAxis(AXIS_3_NAME);

		iCount = 0;
		Timecounter.restart();
		while (Timecounter.elapsed_msec() < iDuration_ms)
		{
			mctrlh.Read();
			iCount++;
		}
		cout << "Loops: "    << iCount << endl;
		cout << "Duration: " << Timecounter.elapsed_msec() << " ms"<<endl;
		cout << "Time for one iteration: " << Timecounter.elapsed_msec() / iCount << " ms" << endl << endl;

		// Read 1 attribute Loop during 1 second		
		cout << "-- MotionControlHelper: Read (sync for 1 attribute) for " << iDuration_ms  << " milli seconds ..." << endl;
		mctrlh.Clear();
		mctrlh.SelectAxis(AXIS_1_NAME);
		iCount = 0;
		Timecounter.restart();
		while (Timecounter.elapsed_msec() < iDuration_ms)
		{
			mctrlh[AXIS_1_NAME]->Read();
			iCount++;
		}

		cout << "Loops: "    << iCount << endl;
		cout << "Duration: " << Timecounter.elapsed_msec() << " ms"<<endl;
		cout << "Time for one iteration: " << Timecounter.elapsed_msec() / iCount << " ms" << endl << endl;

		// Write Loop during 1 seconds        
		cout << "-- MotionControlHelper: Write (1 attribute) for " << iDuration_ms << " milli seconds ..." << endl;
		mctrlh.Clear();
		mctrlh.SelectAxis(AXIS_2_NAME);
		Timecounter.restart();
		iCount = 0;
		int iRandValue = 0.0;
		while (Timecounter.elapsed_msec() < iDuration_ms)
		{
			iRandValue++ ;
			mctrlh[AXIS_2_NAME]->SetWrite(iRandValue);
			mctrlh.Move();
			iCount++;
		}
		cout << "Loops: "    << iCount << endl;
		cout << "Duration: " << Timecounter.elapsed_msec() << " ms"<<endl;
		cout << "Time for one iteration: " << Timecounter.elapsed_msec() / iCount << " ms" << endl << endl;

		mctrlh.Stop();


		/////////////////////////////////////////////////////////
		// Unselect
		// Definit les axes qu'on veut utiliser pour le "write"
		sleep(1);
		cout << "\nSelect Axis (3 axis)" << endl;
		cout << "----------------------------------------------" << endl;				
		mctrlh.Clear();		
		mctrlh.SelectAxis(AXIS_1_NAME);
		mctrlh.SelectAxis(AXIS_2_NAME);
		mctrlh.SelectAxis(AXIS_3_NAME);
		
		// Choisit les valeurs à écrire
		cout << "\nSet values (3 axes)" << endl;
		cout << "----------------------------------------------" << endl;
		mctrlh[AXIS_1_NAME]->SetWrite(1);
		mctrlh[AXIS_2_NAME]->SetWrite(2);
		mctrlh[AXIS_3_NAME]->SetWrite(3);		
		
		cout << "\nMove all Selected axes (3 axes)" << endl;
		cout << "----------------------------------------------" << endl;
		// Déclenche la création et l'écriture sur l'attributegroup sous jacent
		mctrlh.Move();

		Timecounter.restart();	
		cout << "\nUnSelect Axis (1 & 3)" << endl;
		cout << "----------------------------------------------" << endl;			
		mctrlh.UnSelectAxis(AXIS_1_NAME);
		mctrlh.UnSelectAxis(AXIS_3_NAME);
		cout << "Duration UnSelectAxis (2 axis): " << Timecounter.elapsed_msec() << " ms"<<endl;
		
		// Change value to write to Axis 2
		mctrlh[AXIS_2_NAME]->SetWrite(1);

		cout << "\nMove all Selected axes (1 axis)" << endl;
		cout << "----------------------------------------------" << endl;

		// Déclenche la création et l'écriture sur l'attributegroup sous jacent
		mctrlh.Move();
		/////////////////////////////////////////////////////////        

		cout << "[END]" << endl;
	}
	catch (Tango::DevFailed& df)
	{
		cout << "main() / catch (Tango::DevFailed& df)" << endl;
		for(unsigned i = 0;i < df.errors.length();i++)
		{
			cout << "Origin\t: " << df.errors[i].origin << endl;
			cout << "Desc\t: "   << df.errors[i].desc   << endl;
			cout << "Reason\t: " << df.errors[i].reason << endl;
		}
		return -1;
	}
	catch (...)
	{
		cout << "Unknown error from MotionControlHelper" << endl;
		return -1;
	}
	return 0;
}
