from conan import ConanFile

class motioncontrolhelperRecipe(ConanFile):
    name = "motioncontrolhelper"
    version = "1.2.0"
    package_type = "library"
    user = "soleil"
    python_requires = "base/[>=1.0]@soleil/stable"
    python_requires_extend = "base.Library"
    
    license = "GPL-2"
    author = "Bouladoux", "Nourreddine"
    url = "https://gitlab.synchrotron-soleil.fr/software-control-system/libraries/motioncontrolhelper"
    description = "MotionControlHelper library"
    topics = ("utility", "control-system")

    settings = "os", "compiler", "build_type", "arch"
    options = {"shared": [True, False], "fPIC": [True, False]}
    default_options = {"shared": False, "fPIC": True}

    exports_sources = "CMakeLists.txt", "src/**"

    def requirements(self):
        self.requires("yat4tango/[>=1.0]@soleil/stable")
