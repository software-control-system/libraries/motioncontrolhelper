cmake_minimum_required(VERSION 3.15)
project (motioncontrolhelper)

option(BUILD_SHARED_LIBS "Build using shared libraries" ON)

find_package(yat4tango CONFIG REQUIRED)

add_compile_definitions(
    PROJECT_NAME=${PROJECT_NAME}
    PROJECT_VERSION=${PROJECT_VERSION}
)

file(GLOB_RECURSE sources
    src/*.cpp
)

set(includedirs 
    src
)

add_library(motioncontrolhelper ${sources})
target_include_directories(motioncontrolhelper PRIVATE ${includedirs})
target_link_libraries(motioncontrolhelper PRIVATE yat4tango::yat4tango)

if(MAJOR_VERSION)
    set_target_properties(motioncontrolhelper PROPERTIES VERSION ${PROJECT_VERSION} SOVERSION ${MAJOR_VERSION})
endif()

install (DIRECTORY ${CMAKE_SOURCE_DIR}/src/ DESTINATION include
    FILES_MATCHING PATTERN "*.h"
)

install (TARGETS motioncontrolhelper LIBRARY DESTINATION ${LIB_INSTALL_DIR})

